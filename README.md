# tailwind-test

https://tailwindcss.com/

## Setup
```
npm install
```

```
npm run serve (for dev env)
```

```
npm run build (for prod env)
```

### Description
This small project demoes tailwind and purgecss configuration in Vue 

To setup in other frameworks please refer to 

https://medium.com/backticks-tildes/how-to-configure-your-vue-js-app-to-use-tailwind-css-a6f95d06e1c7

https://medium.com/backticks-tildes/how-to-configure-your-vue-js-app-to-use-tailwind-css-a6f95d06e1c7

https://www.smashingmagazine.com/2020/02/tailwindcss-react-project/


